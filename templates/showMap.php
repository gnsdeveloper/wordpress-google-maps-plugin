<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<link rel="stylesheet" href="<?php echo plugin_dir_url('/map/css/map.css').'map.css'; ?>">

<script>
    if(typeof google == 'undefined') {
        document.write("\<script src='https://maps.googleapis.com/maps/api/js?v=3.14&libraries=places&sensor=false' type='text/javascript'>\<\/script>");
    }
</script>

<?php

echo  '<div class="container all-maps-plugin">
       <h2>'.__( "All Maps", "wp-mapgenerator" ).'<a href="'.get_admin_url().'admin.php?page=add-map" class="btn btn-primary active pull-right" role="button">Add Map</a></h2>
       <div class="row">';

if(!empty($result)) {

    echo '<table class="col-sm-11 table-striped table-bordered">
       <thead><tr class="text-center"><td>'.__( "Number", "wp-mapgenerator" ).'</td><td>'.__( "View", "wp-mapgenerator" ).'</td><td>'.__( "Address", "wp-mapgenerator" ).'</td><td>'.__( "Map Name", "wp-mapgenerator" ).'</td><td>'.__( "Color", "wp-mapgenerator" ).'</td><td>'.__( "Shortcode", "wp-mapgenerator" ).'</td><td></td></tr></thead>
       <tbody>';

    foreach ($result as $key => $map) { ?>
    <tr class="text-center">
        <td class="col-sm-1"><?php echo ($key + 1); ?></td>
        <td class="col-sm-2" ><?php echo do_shortcode($map->shortcode); ?></td>
        <td class="col-sm-2"><?php echo  $map->address; ?></td>
        <td class="col-sm-1"><?php echo  $map->map_name; ?></td>
        <td class="col-sm-1"><?php echo  $map->color; ?></td>
        <td class="col-sm-2"><input type="text" readonly value='<?php echo $map->shortcode; ?>' /></td>
        <?php
        echo '<td class="col-sm-2"><form name="edit-map" class="pull-left edit-map">
                               <input class="number" type="hidden" value="' . $map->id . '" />
                               <input type="submit" class="btn btn-success" value="'. __( "Edit", "wp-mapgenerator" ).'" /></form>
                               <form name="edit-map" class="pull-right delete-map">
                               <input class="number" type="hidden" value="' . $map->id . '" />
                               <input type="submit" class="btn btn-danger" value="'. __( "Delete", "wp-mapgenerator" ).'" /></form>
                               </td>';
        echo '</td></tr>';
    }

    echo '</tbody></table>';

}
else {
    echo '<p>'. __( "No maps created yet, ", "wp-mapgenerator" ).'<a href="'.get_admin_url().'admin.php?page=add-map">'. __( "click here", "wp-mapgenerator" ).'</a> '. __( "to create a new one", "wp-mapgenerator" ).'</p>';
}

echo  '</div></div>';

?>

