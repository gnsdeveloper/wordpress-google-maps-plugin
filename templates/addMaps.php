<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<script>
    if(typeof google == 'undefined') {
        document.write("\<script src='https://maps.googleapis.com/maps/api/js?v=3.14&libraries=places&sensor=false' type='text/javascript'>\<\/script>");
    }
</script>
<?php

$address = '';
$color = '1';
$name = '';
$wigth = 611;
$height = 500;
$text = '';

$mapPatterns = (new Map())->getAllMapPattern();

if(!empty($_GET['edit']))
{
    $mapInfo = (new Map())->getMapInfo($_GET['edit']);

    $address = $mapInfo[0]->address;
    $color = $mapInfo[0]->color;
    $name = $mapInfo[0]->map_name;
    $wigth = $mapInfo[0]->map_width;
    $height = $mapInfo[0]->map_height;
    $text = $mapInfo[0]->map_text;
}

echo '<div class="container">
           <h2>'. __( "Add Maps", "wp-mapgenerator" ).'</h2>
           <div class="row">
               <div id="newMap" class="col-sm-5">
                   <form name="add-map" class="add-map">
                       <div class="map-info"><label for="address">'. __( "Address: ", "wp-mapgenerator" ).'</label>
                           <input id="address" type="text" class="mapAddress" value="'.$address.'" />
                       </div>
                       <div class="map-info"><label for="map-name">'. __( "Map Name: ", "wp-mapgenerator" ).'</label>
                           <input id="map-name" type="text" class="mapName" value="'.$name.'" />
                       </div>
                       <div class="map-info"><label for="map-width">'. __( "Map Width: ", "wp-mapgenerator" ).'</label>
                           <input id="map-width" type="number" class="mapWidth" value="'.$wigth.'" /> px
                       </div>
                       <div class="map-info"><label for="map-height">'. __( "Map Height: ", "wp-mapgenerator" ).'</label>
                           <input id="map-height" type="number" class="mapHeight" value="'.$height.'" /> px
                       </div>
                       <div class="map-info"><label class="custom-valign-top" for="map-text">'. __( "Map Text: ", "wp-mapgenerator" ).'</label>
                           <textarea id="map-text" class="mapText" />'.$text.'</textarea>
                       </div>
                           <input id="color" type="hidden" value="'.$color.'" class="mapColor" /></label>
                       <div class="map-info">
                           <input type="button" class="btn btn-primary pull-right save-map" value="'. __( "Save", "wp-mapgenerator" ).'" />
                       </div>
                   </form>
               </div>
               <div class="pre-map col-sm-7">
                   <div id="map"></div>
               </div>
               <div class="map-patterns">';
                    foreach($mapPatterns as $pattern) {
                        echo '<div class="pattern col-sm-4">
                                 <img src="'.plugin_dir_url( __DIR__ ).'/images/pattern_'.$pattern->id.'.png" pattern-number="'.$pattern->id.'" class="pattern-img col-sm-12" />
                             </div>';
                    }
                    echo '</div>
           </div>
       </div>';

?>