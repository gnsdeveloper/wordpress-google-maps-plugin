<?php
/*
Plugin Name: WP-Mapgenerator
Plugin URI:
Description: Plugin for finding location on map
Author:
Version: 1.0
Author URI:
Text Domain: wp-mapgenerator
Domain Path: /languages/
*/

if (!class_exists('Map')) {

    class Map
    {

        public $table_1 = 'wp_map';
        public $table_2 = 'wp_map_pattern';

        function __construct()
        {
            // add Google Api
            add_action('admin_enqueue_scripts', array($this, 'includeScriptAndStyle'));

            // add new menu
            add_action('admin_menu', array($this, 'addMapMenu'));

            // Ajax hook for Edit Map
            add_action('wp_ajax_edit_map', array($this, 'editMap'));
            add_action('wp_ajax_nopriv_edit_map', array($this, 'editMap'));

            // Ajax hook for Delete Map
            add_action('wp_ajax_delete_map', array($this, 'deleteMap'));
            add_action('wp_ajax_nopriv_delete_map', array($this, 'deleteMap'));

            // Ajax hook for Delete Map
            add_action('wp_ajax_pattern_map', array($this, 'getPatternMap'));
            add_action('wp_ajax_nopriv_pattern_map', array($this, 'getPatternMap'));

            // Ajax hook for Preview Map
            add_action('wp_ajax_preview_map', array($this, 'previewMap'));
            add_action('wp_ajax_nopriv_preview_map', array($this, 'previewMap'));

            // Ajax hook for Save Map
            add_action('wp_ajax_save_map', array($this, 'saveMap'));
            add_action('wp_ajax_nopriv_save_map', array($this, 'saveMap'));

            // Registered shortcode
            add_shortcode('mapcode', array($this, 'shortcodeMap'));

            // Use shortcode without "do_shortcode()"
            add_filter('widget_text', 'shortcode_unautop');
            add_filter('widget_text', 'do_shortcode');

            //add translations
            load_plugin_textdomain( 'wp-mapgenerator', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

            add_action('init', array($this, 'pluginUpdate'));

            $this->checkTable();
        }

        function pluginUpdate() {
            require_once ('wp_autoupdate.php');
            $wptuts_plugin_current_version = '1.0';
            //path to plugin
            $wptuts_plugin_remote_path = 'http://localhost/update.php';
            $wptuts_plugin_slug = plugin_basename(__FILE__);
            new wp_auto_update ($wptuts_plugin_current_version, $wptuts_plugin_remote_path, $wptuts_plugin_slug);
        }

        function nl2br2($string) {
            $string = str_replace(array("\r\n", "\r", "\n"), "<br>", $string);
            return $string;
        }

        // Add styles and scripts
        function includeScriptAndStyle($hook_suffix)
        {
            // first check that $hook_suffix is appropriate for your admin page
            wp_enqueue_script(
                'mapgenerator',
                plugins_url('js/map.js', __FILE__),
                array('jquery'),
                false,
                true
            );

            // add plugin style
            wp_register_style('mapStyle', plugins_url('css/map.css', __FILE__));
            wp_enqueue_style('mapStyle');
        }

        function google_api_script()
        {
            wp_enqueue_script(
                'google-maps',
                'https://maps.googleapis.com/maps/api/js?v=3.14&libraries=places'
            );
        }

        function shortcodeMap($atts)
        {

            add_action('admin_enqueue_scripts', array($this, 'google_api_script'));

            global $wpdb;

            ob_start();

            $id = $atts['id'];
            $result = $wpdb->get_results(
                $wpdb->prepare("SELECT address, color, map_width, map_height, map_text, link_id FROM wp_map WHERE id = %d", $id)
            );
            $address = $result[0]->address;
            $width = $result[0]->map_width;
            $height = $result[0]->map_height;
            $text = $this->nl2br2($result[0]->map_text);

            $color = $this->getPatternMap($result[0]->color);

            $mapInfo = $this->previewMap('1', $address);

            $link = $result = $wpdb->get_row(
                $wpdb->prepare("SELECT * FROM wp_map_links WHERE id = %d", $result[0]->link_id)
            );

            $textLink = $link->link_text;
            $mapLink = $link->link;

            ob_get_clean();

            ?>
            <script>
                if (typeof google == 'undefined') {
                    document.write("\<script src='https://maps.googleapis.com/maps/api/js?v=3.14' type='text/javascript'>\<\/script>");
                }
            </script>

            <script>

                function initialize() {

                    var markerText = '';
                    var colorPhp = <?php echo $color; ?>;
                    var tempText = '<?php echo $text; ?>';

                    if (tempText != '') {
                        markerText = '<?php echo $text; ?>';
                    }

                    var styles = colorPhp;

                    var myLatlng = new google.maps.LatLng('<?php echo $mapInfo['lat']; ?>', '<?php echo $mapInfo['long']; ?>');

                    var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});

                    var mapOptions = {
                        zoom: 13,
                        center: myLatlng,
                        mapTypeControlOptions: {
                            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                        }

                    };
                    var map = new google.maps.Map(document.getElementById('map_<?php echo $id; ?>'), mapOptions);

                    map.mapTypes.set('map_style', styledMap);
                    map.setMapTypeId('map_style');

                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        title: 'This is a caption'
                    });

                    if (markerText != '') {
                        var infoWindow = new google.maps.InfoWindow({
                            content: markerText
                        });

                        google.maps.event.addListener(marker, "click", function (e) {
                            infoWindow.open(map, this);
                        });

                        infoWindow.open(map, marker);
                    }
                }


                google.maps.event.addDomListener(window, 'load', initialize);
            </script>

            <a class="custom-link-map" href="<?php echo $mapLink; ?>">
                <span><?php echo $textLink; ?></span>
            </a>
            <div id="map_<?php echo $id; ?>" class="google-map-cart"
                 style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px;"></div>

            <?php
        }

        //Add new menu tab for plugin
        function addMapMenu()
        {
            add_menu_page('Maps Page', 'Maps', 'manage_options', 'show-map', array($this, 'showMapPage'));
            add_submenu_page('show-map', 'Add Map', 'Add Map', 'manage_options', 'add-map', array($this, 'addMapPage'));
        }

        function saveMap()
        {
            $id = strip_tags($_POST['id']);

            $address = strip_tags($_POST['address']);
            $color = $_POST['color'] ? strip_tags($_POST['color']) : 'none';
            $name = $_POST['name'] ? strip_tags($_POST['name']) : 'map';
            $width = $_POST['width'] ? strip_tags($_POST['width']) : 0;
            $height = $_POST['height'] ? strip_tags($_POST['height']) : 0;
            $text = $_POST['text'] ? strip_tags($_POST['text']) : '';

            // edit exists map
            if (!empty($id)) {
                $this->editMap($id, $address, $color, $name, $width, $height, $text);
                die;
            }

            global $wpdb;

            $message = array("error" => 1);

            if (!empty($address) && !empty($color)) {
                // get last insert id
                $last = $wpdb->get_row("SHOW TABLE STATUS LIKE 'wp_map'");
                $shortcode = '[mapcode id="'.$last->Auto_increment.'"]';
                $linkId = $wpdb->get_var('SELECT id FROM wp_map_links ORDER BY RAND() LIMIT 1');

                $wpdb->query(
                    $wpdb->prepare(
                        "INSERT INTO wp_map
                        ( address, color, map_name, map_width, map_height, map_text, shortcode, link_id )
                        VALUES ( %s, %s, %s, %d, %d, %s, %s, %d )
                    ",
                        $address,
                        $color,
                        $name,
                        $width,
                        $height,
                        $text,
                        $shortcode,
                        $linkId
                    )
                );

                $message = array("error" => 0);
            }

            echo json_encode($message);
            die();
        }

        function previewMap($view = '0', $address = '0')
        {
            if ($address == '0') {
                $address = $_POST['address'];
            }

            if ($_POST['color'] == '' || $_POST['color'] == 'none') {
                $color = 'none';
            } else {
                $color = $this->getPatternMap($_POST['color']);
            }

            $addressGoogleFormat = $this->convertAddress($address);

            $url = "http://maps.google.com/maps/api/geocode/json?address=$addressGoogleFormat";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);

            $response_a = json_decode($response);
            $lat = $response_a->results[0]->geometry->location->lat;
            $long = $response_a->results[0]->geometry->location->lng;

            if ($view == 1) {
                $message = array("lat" => $lat, "long" => $long);

                return $message;
            } else {
                $message = array("error" => 0, "lat" => $lat, "long" => $long, "color" => $color);
                echo json_encode($message);
                die();
            }
        }

        function convertAddress($address)
        {
            return str_replace(array(', ', ' '), '+', $address);
        }

        // Edit Map
        function editMap($id, $address, $color, $name, $width, $height, $text)
        {
            global $wpdb;

            $wpdb->query(
                $wpdb->prepare(
                    "UPDATE wp_map
                         SET address = %s,
                             color = %s,
                             map_name = %s,
                             map_width = %s,
                             map_height = %s,
                             map_text = %s
		                 WHERE id = %d",
                    $address,
                    $color,
                    $name,
                    $width,
                    $height,
                    $text,
                    $id
                )
            );

            $message = array("error" => 0);

            echo json_encode($message);
            die();
        }

        // Delete Map
        function deleteMap()
        {
            global $wpdb;

            $id = strip_tags($_POST['id']);

            $message = array("error" => 1);

            if (!empty($id) && is_numeric($id)) {
                $wpdb->query(
                    $wpdb->prepare(
                        "DELETE FROM wp_map
		                 WHERE id = %d",
                        $id
                    )
                );

                $message = array("error" => 0);
            }


            echo json_encode($message);
            die();
        }

        function getMapInfo($id)
        {

            global $wpdb;

            return $wpdb->get_results(
                $wpdb->prepare(
                    "SELECT address, color, map_name, map_width, map_height, map_text FROM wp_map WHERE id = %d",
                    $id
                )
            );

        }

        function getPatternMap($id = 0)
        {

            if ($id == 'none') {
                return $result = [];
            }

            global $wpdb;

            if (isset($_POST["patternId"])) {
                $id = $_POST["patternId"];
            }

            $result = $wpdb->get_results($wpdb->prepare("SELECT ajax_color FROM wp_map_pattern WHERE id = %d", $id));

            if (isset($_POST["patternId"])) {
                $message = array("error" => 0, "color" => $result[0]->ajax_color);

                echo json_encode($message);
                die();
            }

            return $result[0]->ajax_color;

        }

        function getAllMapPattern()
        {

            global $wpdb;

            return $wpdb->get_results("SELECT * FROM wp_map_pattern");

        }

        // Template for creation new Map
        function addMapPage()
        {
            require_once(plugin_dir_path(__FILE__).'templates/addMaps.php');
        }

        // Template with all Maps
        function showMapPage()
        {

            global $wpdb;

            $result = $wpdb->get_results("SELECT * FROM wp_map");

            require_once(plugin_dir_path(__FILE__).'templates/showMap.php');
        }

        // check if not exists table in DB
        function checkTable()
        {

            global $wpdb;

            $table_1 = $this->table_1;
            $table_2 = $this->table_2;

            if ($wpdb->get_var("SHOW TABLES LIKE '$table_1'") != $table_1 || $wpdb->get_var(
                    "SHOW TABLES LIKE '$table_2'"
                ) != $table_2
            ) {
                $this->createTable();
            }
        }

        // create table in DB if not exists
        function createTable()
        {

            global $wpdb;

            $table_name = $wpdb->prefix.'map';

            $charset_collate = $wpdb->get_charset_collate();

            $sql = "CREATE TABLE $table_name (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
              address VARCHAR(100) NOT NULL,
              color VARCHAR(100) NOT NULL,
              map_name VARCHAR(100) NOT NULL,
              map_width VARCHAR(100) NOT NULL,
              map_height VARCHAR(100) NOT NULL,
              map_text VARCHAR(100) NOT NULL,
              shortcode VARCHAR(100) NOT NULL,
              link_id VARCHAR(100) NOT NULL,
              UNIQUE KEY id (id)
            ) $charset_collate;";

            $charset_collate_2 = $wpdb->get_charset_collate();

            $wpPatternMap = "CREATE TABLE wp_map_pattern (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
              ajax_color TEXT NOT NULL,
              UNIQUE KEY id (id)
            ) $charset_collate_2;";

            $charset_collate_3 = $wpdb->get_charset_collate();

            $wpLinkMap = "CREATE TABLE wp_map_links (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
              link VARCHAR(100) NOT NULL,
              link_text VARCHAR(100) NOT NULL,
              UNIQUE KEY id (id)
            ) $charset_collate_3;";

            require_once(ABSPATH.'wp-admin/includes/upgrade.php');
            dbDelta($sql);
            dbDelta($wpPatternMap);
            dbDelta($wpLinkMap);

            global $wpdb;

            // insert default value to DB
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 1,
                    'ajax_color' => '[{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]'
                )
            );
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 2,
                    'ajax_color' => '[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]'
                )
            );
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 3,
                    'ajax_color' => '[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]'
                )
            );
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 4,
                    'ajax_color' => '[{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2e5d4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]}]'
                )
            );
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 5,
                    'ajax_color' => '[{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]'
                )
            );
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 6,
                    'ajax_color' => '[{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#e0efef"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#1900ff"},{"color":"#c0e8e8"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":700}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#7dcdcd"}]}]'
                )
            );
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 7,
                    'ajax_color' => '[{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]'
                )
            );
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 8,
                    'ajax_color' => '[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"color":"#000000"},{"lightness":13}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#144b53"},{"lightness":14},{"weight":1.4}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#08304b"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#0c4152"},{"lightness":5}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#0b434f"},{"lightness":25}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#0b3d51"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"transit","elementType":"all","stylers":[{"color":"#146474"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#021019"}]}]'
                )
            );
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 9,
                    'ajax_color' => '[{"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#2c2e33"},{"saturation":7},{"lightness":19},{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":-2},{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#e9ebed"},{"saturation":-90},{"lightness":-8},{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":10},{"lightness":69},{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":-78},{"lightness":67},{"visibility":"simplified"}]}]'
                )
            );
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 10,
                    'ajax_color' => '[{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"simplified"},{"hue":"#0066ff"},{"saturation":74},{"lightness":100}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"off"},{"weight":0.6},{"saturation":-85},{"lightness":61}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"simplified"},{"color":"#5f94ff"},{"lightness":26},{"gamma":5.86}]}]'
                )
            );
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 11,
                    'ajax_color' => '[{"featureType":"landscape","stylers":[{"hue":"#FFBB00"},{"saturation":43.400000000000006},{"lightness":37.599999999999994},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#FFC200"},{"saturation":-61.8},{"lightness":45.599999999999994},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#0078FF"},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#00FF6A"},{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]}]'
                )
            );
            $wpdb->insert(
                'wp_map_pattern',
                array(
                    'id' => 12,
                    'ajax_color' => '[{"featureType":"administrative","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"visibility":"off"}]},{"featureType":"road.local","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"water","stylers":[{"color":"#84afa3"},{"lightness":52}]},{"stylers":[{"saturation":-17},{"gamma":0.36}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#3f518c"}]}]'
                )
            );

            //custom link for map
            $wpdb->insert(
                'wp_map_links',
                array(
                    'id' => 1,
                    'link' => 'https://www.google.com/1',
                    'link_text' => 'text_custom_link_1'
                )
            );
            $wpdb->insert(
                'wp_map_links',
                array(
                    'id' => 2,
                    'link' => 'https://www.google.com/2',
                    'link_text' => 'text_custom_link_2'
                )
            );
            $wpdb->insert(
                'wp_map_links',
                array(
                    'id' => 3,
                    'link' => 'https://www.google.com/3',
                    'link_text' => 'text_custom_link_3'
                )
            );
            $wpdb->insert(
                'wp_map_links',
                array(
                    'id' => 4,
                    'link' => 'https://www.google.com/4',
                    'link_text' => 'text_custom_link_4'
                )
            );
            $wpdb->insert(
                'wp_map_links',
                array(
                    'id' => 5,
                    'link' => 'https://www.google.com/5',
                    'link_text' => 'text_custom_link_5'
                )
            );
            $wpdb->insert(
                'wp_map_links',
                array(
                    'id' => 6,
                    'link' => 'https://www.google.com/6',
                    'link_text' => 'text_custom_link_6'
                )
            );
            $wpdb->insert(
                'wp_map_links',
                array(
                    'id' => 7,
                    'link' => 'https://www.google.com/7',
                    'link_text' => 'text_custom_link_7'
                )
            );
            $wpdb->insert(
                'wp_map_links',
                array(
                    'id' => 8,
                    'link' => 'https://www.google.com/8',
                    'link_text' => 'text_custom_link_8'
                )
            );
            $wpdb->insert(
                'wp_map_links',
                array(
                    'id' => 9,
                    'link' => 'https://www.google.com/9',
                    'link_text' => 'text_custom_link_9'
                )
            );
            $wpdb->insert(
                'wp_map_links',
                array(
                    'id' => 10,
                    'link' => 'https://www.google.com/10',
                    'link_text' => 'text_custom_link_10'
                )
            );

        }

    }

    new Map();

}